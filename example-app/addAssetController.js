import SessionManager from 'session-manager';
import AssetServiceSdk from 'asset-service-sdk';
import {AddAssetModalConfig} from '../src/index';

export default class AddAssetController {

    _sessionManager:SessionManager;

    _addAssetModal;

    _assetServiceSdk:AssetServiceSdk;

    _assetSerialNumber;

    _assetAccountId;

    _asset;

    constructor(sessionManager:SessionManager,
                addAssetModal,
                assetServiceSdk:AssetServiceSdk) {

        if (!sessionManager) {
            throw new TypeError('sessionManager required');
        }
        this._sessionManager = sessionManager;

        if (!addAssetModal) {
            throw new TypeError('addAssetModal required');
        }
        this._addAssetModal = addAssetModal;

        if (!assetServiceSdk) {
            throw new TypeError('assetServiceSdk required');
        }
        this._assetServiceSdk = assetServiceSdk;

    }

    get assetSerialNumber() {
        return this._assetSerialNumber;
    }

    set assetSerialNumber(value) {
        this._assetSerialNumber = value;
    }

    get assetAccountId() {
        return this._assetAccountId;
    }

    set assetAccountId(value) {
        this._assetAccountId = value;
    }

    get asset() {
        return this._asset;
    }

    showAddAssetModal() {

        this._addAssetModal
            .show(
                new AddAssetModalConfig(
                    this._assetSerialNumber,
                    this._assetAccountId
                )
            )
            .then(assetId => {
                    this._asset = {id: assetId};
                }
            )
            .then(() => this._sessionManager.getAccessToken())
            .then(accessToken =>
                this._assetServiceSdk
                    .getAssetsWithIds(
                        [this._asset.id],
                        accessToken
                    )
            )
            .then(assets => {
                    this._asset = assets[0];
                }
            );

    }

}


AddAssetController.$inject = [
    'sessionManager',
    'addAssetModal',
    'assetServiceSdk'
];

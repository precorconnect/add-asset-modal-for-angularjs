import addAssetTemplate from './add-asset.html!text';
import AddAssetController from './addAssetController';

export default class RouteConfig {

    constructor($routeProvider) {

        $routeProvider
            .when
            (
                '/',
                {
                    template: addAssetTemplate,
                    controller: AddAssetController,
                    controllerAs: 'controller'
                }
            );

    }

}

RouteConfig.$inject = [
    '$routeProvider'
];
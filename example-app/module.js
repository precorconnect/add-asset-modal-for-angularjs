import angular from 'angular';
import '../src/index';
import RouteConfig from './routeConfig';
import SessionManager,{SessionManagerConfig} from 'session-manager';
import AssetServiceSdk,{AssetServiceSdkConfig} from 'asset-service-sdk';
import ProductLineServiceSdk,{ProductLineServiceSdkConfig} from 'product-line-service-sdk'
import 'angular-route';

const precorConnectApiBaseUrl = 'https://api-dev.precorconnect.com';

const sessionManager =
    new SessionManager(
        new SessionManagerConfig(
            precorConnectApiBaseUrl/* identityServiceBaseUrl */,
            'https://precor.oktapreview.com/app/template_saml_2_0/exk5cmdj3pY2eT5JU0h7/sso/saml'/* loginUrl */,
            'https://dev.precorconnect.com/customer/asset/logout/'/* logoutUrl*/,
            30000/* accessTokenRefreshInterval */
        )
    );

const assetServiceSdk =
    new AssetServiceSdk(
        new AssetServiceSdkConfig(
            precorConnectApiBaseUrl
        )
    );

const productLineServiceSdk =
    new ProductLineServiceSdk(
        new ProductLineServiceSdkConfig(
            precorConnectApiBaseUrl
        )
    );

angular
    .module(
        'exampleApp.module',
        [
            'ngRoute',
            'addAssetModal.module'
        ]
    )
    .constant(
        'sessionManager',
        sessionManager
    )
    .constant(
        'assetServiceSdk',
        assetServiceSdk
    )
    .constant(
        'productLineServiceSdk',
        productLineServiceSdk
    )
    .config(
        [
            '$routeProvider',
            $routeProvider => new RouteConfig($routeProvider)
        ]
    );

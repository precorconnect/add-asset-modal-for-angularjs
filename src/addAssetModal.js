import Controller from './controller';
import './default.css!css';
import template from './index.html!text';
import AddAssetModalConfig from './addAssetModalConfig';

export default class AddAssetModal {

    _$modal;

    constructor($modal) {

        if (!$modal) {
            throw new TypeError('$modal required');
        }
        this._$modal = $modal;

    }

    /**
     * Shows the add asset modal
     * @param {AddAssetModalConfig} config
     * @returns {Promise<string>} asset id
     */
    show(config:AddAssetModalConfig):Promise<string> {

        let modalInstance = this._$modal.open({
            controller: Controller,
            controllerAs: 'controller',
            template: template,
            backdrop: 'static',
            resolve: {
                config: function () {
                    return config;
                }
            }
        });

        return modalInstance.result;
    }
}

AddAssetModal.$inject = [
    '$modal'
];
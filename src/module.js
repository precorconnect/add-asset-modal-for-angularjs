import angular from 'angular';
import AddAssetModal from './addAssetModal';
import errorMessagesTemplate from './error-messages.html!text';
import 'angular-bootstrap';
import 'angular-messages';
import 'bootstrap/css/bootstrap.css!css'
import 'bootstrap'

angular
    .module(
        'addAssetModal.module',
        [
            'ui.bootstrap',
            'ngMessages'
        ]
    )
    .service(
        'addAssetModal',
        [
            '$modal',
            AddAssetModal
        ]
    )
    .run(
        [
            '$templateCache',
            $templateCache => {
                $templateCache.put(
                    'add-asset-modal/error-messages.html',
                    errorMessagesTemplate
                );
            }
        ]
    );
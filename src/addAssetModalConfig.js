export default class AddAssetModalConfig {

    _assetSerialNumber:string;

    _assetAccountId:string;

    constructor(assetSerialNumber:string,
                assetAccountId:string) {

        if (!assetSerialNumber) {
            throw 'assetSerialNumber required';
        }
        this._assetSerialNumber = assetSerialNumber;

        if (!assetAccountId) {
            throw 'assetAccountId required';
        }
        this._assetAccountId = assetAccountId;

    }

    get assetSerialNumber():string {
        return this._assetSerialNumber;
    }

    get assetAccountId():string {
        return this._assetAccountId;
    }

}

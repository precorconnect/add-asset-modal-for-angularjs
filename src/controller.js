import angular from 'angular';
import AssetServiceSdk,{AddAssetReq} from 'asset-service-sdk';
import ProductLineServiceSdk from 'product-line-service-sdk';
import SessionManager from 'session-manager';
import AddAssetModalConfig from './addAssetModalConfig';

export default class Controller {

    _config:AddAssetModalConfig;

    _sessionManager:SessionManager;

    _$modalInstance;

    _assetServiceSdk:AssetServiceSdk;

    _productLineList;

    _addAssetReqData;

    constructor(sessionManager:SessionManager,
                $q,
                $modalInstance,
                assetServiceSdk:AssetServiceSdk,
                productLineServiceSdk:ProductLineServiceSdk,
                config:AddAssetModalConfig) {

        if (!sessionManager) {
            throw new TypeError('sessionManager required');
        }
        this._sessionManager = sessionManager;

        if (!$q) {
            throw new TypeError('$q required');
        }

        if (!$modalInstance) {
            throw new TypeError('$modalInstance required');
        }
        this._$modalInstance = $modalInstance;

        if (!assetServiceSdk) {
            throw new TypeError('assetServiceSdk required');
        }
        this._assetServiceSdk = assetServiceSdk;

        if (!productLineServiceSdk) {
            throw  new TypeError('productLineServiceSdk required');
        }

        if (!config) {
            throw new TypeError('config required');
        }
        this._config = config;

        $q(
            resolve =>
                resolve(sessionManager.getAccessToken())
        )
            .then(accessToken =>
                productLineServiceSdk.listProductLines(accessToken)
            )
            .then(productLineList => {
                    this._productLineList = productLineList;
                }
            );

        this._addAssetReqData =
        {
            accountId: config.assetAccountId,
            serialNumber: config.assetSerialNumber
        };

    }

    get addAssetReqData() {
        return this._addAssetReqData;
    }

    get productLineList() {
        return this._productLineList;
    }

    get config() {
        return this._config;
    }

    cancel() {
        this._$modalInstance.dismiss();
    }

    saveAndClose(form) {
        if (form.$valid) {

            this._sessionManager
                .getAccessToken()
                .then(
                    accessToken =>
                        this._assetServiceSdk
                            .addAsset(
                                new AddAssetReq(
                                    this.addAssetReqData.accountId,
                                    this.addAssetReqData.productLineId,
                                    this.addAssetReqData.serialNumber,
                                    this.addAssetReqData.description
                                ),
                                accessToken
                            )
                )
                .then(assetId =>
                    this._$modalInstance.close(assetId)
                );

        }
        else {
            form.$setSubmitted();
        }
    }
}

Controller.$inject = [
    'sessionManager',
    '$q',
    '$modalInstance',
    'assetServiceSdk',
    'productLineServiceSdk',
    'config'
];
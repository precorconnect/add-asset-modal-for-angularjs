## Description
Add asset modal for AngularJS.

## Example
refer to the [example app](example-app) for working example code.

## Setup

**install via jspm**  
```shell
jspm install bitbucket:precorconnect/add-asset-modal-for-angularjs
``` 

**import & wire up**
```js
import 'add-asset-modal-for-angularjs';

angular.module(
            "app",
            ["add-asset-modal.module"]
        )
        // ensure dependencies available in container
        .constant(
            'sessionManager', 
            sessionManager
            /*see https://bitbucket.org/precorconnect/session-manager-for-browsers*/
        )
        .constant(
            'assetServiceSdk',
            assetServiceSdk
            /*see https://bitbucket.org/precorconnect/asset-service-sdk-for-javascript*/
        )        
        .constant(
            'productLineServiceSdk',
            productLineServiceSdk
            /*see https://bitbucket.org/precorconnect/product-line-service-sdk-for-javascript*/
        );
```